# Redux Quiz Backend 说明文档

本程序为 Redux Quiz 的前端网站提供了后端的 API。这些 API 提供了对 Markdown 文档的添加、查询和删除功能。

## 1. API 定义

本应用程序一共包含三个 API：

### 1.1 查询所有文档：

查询所有文档的请求格式如下

|项目|值|
|---|---|
|URI|`/api/posts`|
|Http Method|`GET`|

如果查询成功则会得到如下的响应：

|项目|值|
|---|---|
|Content-Type|`application/json;charset=UTF-8`|
|Status Code|`200`|

其中 Content Body 为一个 JSON 数组。其数组中的每一个元素包含如下属性：

|属性|说明|
|---|---|
|`id`|该 Markdown 文档的 id。其类型为整数。|
|`title`|该 Markdown 文档的标题。其类型为字符串，最大长度为 128 个字符|
|`description`|该 Markdown 文档的内容。其类型为字符串。最大长度为 65536 个字符|

范例：

```json
[
  {
    "id": 1,
    "title": "C# 和 .NET Framework 简介",
    "description": "C# 是一种通用的，..."
  },
  {
    "id": 2,
    "title": "编译",
    "description": "C# 编译器将一系列..."
  },
  {
    "id": 3,
    "title": "方法",
    "description": "方法用一组语句实现某个行为。..."
  }
]
```

### 1.2 创建文档

创建文档的请求格式要求如下：

|项目|值|
|---|---|
|URI|`/api/posts`|
|Http Method|`POST`|
|Content-Type|`application/json;charset=UTF-8`|

其中请求的 body 为 JSON 对象，其中需要包含如下的属性：

|属性|说明|
|---|---|
|`title`|该 Markdown 文档的标题。其类型为字符串，最大长度为 128 个字符。不可为空。|
|`description`|该 Markdown 文档的内容。其类型为字符串。最大长度为 65536 个字符。不可为空。|

创建成功后响应的内容如下：

|项目|值|
|---|---|
|Status Code|`201`|

### 1.3 删除文档

删除文档的请求格式要求如下：

|项目|值|
|---|---|
|URI|`/api/posts/:id`|
|Http Method|`DELETE`|

其中 `:id` 即文档列表中每一个文档的 `id` 属性的值。如果删除成功，或者该文档不存在，均会返回 


|项目|值|
|---|---|
|Status Code|`200`|

### 1.4 错误情况

对于任何错误。Response 为非 2xx 代码。并且 Response Body 如下：

|项目|值|
|---|---|
|timestamp|错误发生的本地时间|
|error|错误的具体信息|
|errorType|相关的异常类型信息|

例如：

```json
{
    "timestamp": "2019-07-26T08:54:07.595",
    "error": "Request method 'GET' not supported",
    "errorType": "org.springframework.web.HttpRequestMethodNotSupportedException"
}
```

## 2 启动 Web Service

可以在命令行中执行 `./gradlew bootRun` 启动该应用程序。在应用程序第一次启动的时候会自动创建数据库。数据库文件在工程目录下的 `./public` 下。默认情况下，数据库中会直接插入三条范例数据。而后续的所有 API 请求对数据的更改也会持久化到数据库中。

在 Web 应用程序启动后，默认的 host 地址为 `http://localhost:8080`。如果希望查看数据库内容可以访问 `http://localhost:8080/h2-console`。数据库的地址以及用户名和密码请参见其中的 `application.properties` 文件。

如果你希望重新创建数据库的内容。请先按 `Ctrl+C` 终止程序运行。之后删除 `./public` 目录下的所有文件。然后重新启动 Web 应用程序，数据库就会自动重新创建。